#include <iostream>
using namespace std;

#include "queue.h"
#include "linkedList.h"
#include "stack.h"
#include "utils.h"

#define TWO 2
#define THREE 3
#define FOUR 4
#define FIVE 5
#define SIX 6
#define SEVEN 7
#define EIGHT 8
#define NINE 9
#define TEN 10
void printList(linkedList* head);

int main()
{
    queue* q = new queue;
    linkedList* head = new linkedList;
    stack* s = new stack;
    int* nums = 0;

    initQueue(q, FIVE);

    enqueue(q, NINE);
    enqueue(q, TWO);
    enqueue(q, FOUR);
    enqueue(q, SIX);
    dequeue(q);
    enqueue(q, THREE);

    for (int i = 0; i < q->size; i++)
    {
        cout << "\n " << (q->queueElements)[i];
    }


    head->val = NULL;
    head->next = NULL;

    addToList(&head, FIVE);
    addToList(&head, SIX);
    addToList(&head, SEVEN);
    cout << "\n----------------------\n";
    printList(head);
    head = removeFromList(&head);
    cout << "\n----------------------\n";
    printList(head);
    cout << "\n----------------------\n";
    nums = reverse10(); //includes checks for the stack header and functions
    cout << "Reverse array: \n";
    for (int i = 0; i < TEN; i++)
    {
        cout << nums[i] << " ";
    }
    cleanQueue(q);
    //delete[] head;
    return 0;
}

/*
the function prints the nodes of the list
input: the head of the list
output: None
*/
void printList(linkedList* head)
{
    linkedList* curr = head;
    int i = 1;
    while (curr->next) // when curr == NULL, that is the end of the list, and loop will end (NULL is false)
    {
        cout << "\n " << curr->val;
        curr = curr->next;
        i++;
    }
    if (i == 1)
    {
        printf("The list is empty\n");
    }
}