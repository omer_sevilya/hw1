#include "utils.h"
#include "stack.h"
#include <iostream>

using namespace std;

#define TEN 10

/*
the function reverses the array
input: the array of the nums
	   the size of the array
output: None
*/
void reverse(int* nums, unsigned int size)
{
	stack* temp = new stack;
	initStack(temp);
	for (int i = 0; i < size; i++)
	{
		push(temp, nums[i]);
	}
	for (int i = 0; i < size; i++)
	{
		nums[i] = pop(temp);
	}
	cleanStack(temp);
}

/*
the function gets 10 numbers from the user
and reverses the order of the numbers
input: None
output: pointer to the array
*/
int* reverse10()
{
	int* nums = new int[TEN];
	cout << "Enter 10 numbers: \n";
	for (int i = 0; i < TEN; i++)
	{
		cin >> nums[i];
	}
	reverse(nums, TEN);

	return nums;
}