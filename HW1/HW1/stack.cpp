#include "stack.h"
#include <iostream>

/*
the function adds new element to the end of the stack
input: the head of the stack
	   the new element to add
output: None
*/
void push(stack* s, unsigned int element)
{
	stack* newNode = new stack;
	newNode->value = element;
	newNode->next = NULL;
	if (s == NULL)
	{
		s = newNode;
	}
	else
	{
		while (s->next != NULL)
		{
			s = s->next;
		}
		s->next = newNode;
	}
}

/*
the function removes the last element of the stack
input: The head of the stack
output: The last element of the stack
*/
int pop(stack* s)
{
	stack* temp = s;
	stack* freeNode = NULL;
	int num = -1;
	if (temp != NULL)
	{
		while (temp->next->next)
		{
			temp = temp->next;
		}
		num = temp->next->value;
		freeNode = temp->next;
		delete(freeNode);
		temp->next = NULL;
	}
	return num;
}

/*
the function Initializing the stack
input: the head of the stack
output: None
*/
void initStack(stack* s)
{
	s->value = NULL;
	s->next = NULL;
}

/*
the function realeses the memory of the stack
input: The head of the stack
output: None
*/
void cleanStack(stack* s)
{
	delete[] s;
}