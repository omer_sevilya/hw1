#include "linkedList.h"

/*
the function adds new element 
to the top of the linked list
input: the head of the linked list
       the new value to add
output: None
*/
void addToList(linkedList** list, unsigned int newVal)
{
    unsigned int tempVal = 0;
    linkedList* curr = *list;
    linkedList* temp = new linkedList;
    temp->val = newVal;
    temp->next = NULL;

    if (curr == NULL) //empty list
    {
        curr = temp;
    }
    else
    {
        //switching the nodes
        tempVal = curr->val;
        curr->val = temp->val;
        temp->val = tempVal;
        temp->next = curr->next;
        curr->next = temp;
    }
}

/*
the function removes the head 
of the linked list
input: the original head of the linked list
output: the new head of the linked list
*/
linkedList* removeFromList(linkedList** list)
{
	linkedList* temp = new linkedList;
	temp = *list;
	*list = (*list)->next;
	delete(temp);
	return *list;
}