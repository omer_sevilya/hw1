#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>



typedef struct linkedList
{
    unsigned int val;

    linkedList* next;
} linkedList;

void addToList(linkedList **list, unsigned int newVal);
linkedList* removeFromList(linkedList** list);

#endif /* LINKEDLIST_H */