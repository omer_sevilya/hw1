#include "queue.h"

/*
the function initializing the queue
input: the head of the queue
       the size of the queue
output: None
*/
void initQueue(queue* q, unsigned int size)
{
    int i = 0;
    q->size = size;
    q->queueElements = new unsigned int[size];

    for (i = 0; i < size; i++)
    {
        (q->queueElements)[i] = EMPTY_CELL;
    }
}

/*
the function realeses the memory of the queue
input: The head of the queue
output: None
*/
void cleanQueue(queue* q)
{
    delete[] q;
}

/*
the function adds new element to the queue
to the head of the queue
input: the head of the queue
       the element to add
output: None
*/
void enqueue(queue* q, unsigned int newValue)
{
    int i = 0;
    bool emptyCellFound = false;

    for (i = 0; i < q->size && !emptyCellFound; i++)
    {
        if ((q->queueElements)[i] == EMPTY_CELL)
        {
            q->queueElements[i] = newValue;
            emptyCellFound = true;
        }
    }
}

/*
the function removes the head of the queue
input: The head of the queue
input: None
*/
int dequeue(queue* q)
{
    int i = 0;
    int val = 0;

    if ((q->queueElements)[0] == EMPTY_CELL)
    {
        val = EMPTY_QUEUE;
    }
    else
    {
        val = (q->queueElements)[0];
        (q->queueElements)[0] = EMPTY_CELL;

        for (i = 0; i < q->size - 1; i++)
        {
            (q->queueElements)[i] = (q->queueElements)[i + 1];
        }

        (q->queueElements)[q->size - 1] = EMPTY_CELL;
    }

    return val;
}